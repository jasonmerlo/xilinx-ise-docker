## Build Instructions
To build the Docker image,
1. Create an `assets` folder
2. Download Xilinx ISE Design Suite 14.7 Full Installer for Linux (TAR/GZIP) from:
    - https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/archive-ise.html
3. Obtain and download a free ISE WebPACK License from:
    - https://www.xilinx.com/member/forms/license-form.html
4. Move the `.tar` file and `Xilinx.lic` file into the `assets` folder
5. Run `./build.sh`
    - The build will hang after the ISE installation script runs; wait ~30s, then exit using ctrl-c and re-run build.sh.  The build should pick up after the installation step and finish the image build.

## Running The Application
To run the application, simply run the `./run.sh`.


> This work was based on the excellent work by Christian Weickhmann at https://bowfinger.de/blog/2022/07/running-xilinx-ise-14-7-in-docker/

