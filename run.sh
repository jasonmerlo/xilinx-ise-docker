#!/bin/bash

XSOCK=/tmp/.X11-unix
XAUTH=/home/$USER/.Xauthority
GIT_CONFIG=/home/$USER/.gitconfig
LOCAL=/home/$USER/.local

# Verify config files exist, otherwise add an empty files to prevent Docker from
# creating folders as root
if [ ! -e "$GIT_CONFIG" ]; then
  touch "$GIT_CONFIG"
fi
if [ ! -e "$LOCAL" ]; then
  mkdir "$LOCAL"
fi

SSH_DIR=/home/$USER/.ssh

USER_ID=$(id -u)
GROUP_ID=$(id -g)

REF_NAME=$USER/ise_docker
echo $REF_NAME


CONTAINER_NAME=$USER\_ise_docker
echo -e $CONTAINER_NAME

if [[ $@ == *n* ]]; then
    echo "-n flag specified -- creating new image..."
    echo "Removing ${CONTAINER_NAME}..."
    sudo docker rm $CONTAINER_NAME
fi

# Check if container exists, if not, create new one
if [ "$(sudo docker ps -q -a -f name=$CONTAINER_NAME)" ]; then
    echo "Starting existing container $CONTAINER_NAME..."
    sudo docker start $CONTAINER_NAME
    echo "Attaching to existing container $CONTAINER_NAME..."
    sudo docker attach $CONTAINER_NAME
else
echo "Creating new container $CONTAINER_NAME..."
    echo "Starting with additional RUN_ARGS:"
    echo "${RUN_ARGS[@]}"
    sudo docker run \
	 -it \
	 "${RUN_ARGS[@]}" \
	 --volume=$HOME:$HOME:rw \
	 --volume=$XSOCK:$XSOCK:rw \
	 --volume=$XAUTH:$XAUTH:rw \
	 --volume=$SSH_DIR:$SSH_DIR:Z \
	 --volume=$GIT_CONFIG:$GIT_CONFIG:rw \
	 --volume=$LOCAL:$LOCAL:rw \
	 --env="XAUTHORITY=${XAUTH}" \
	 --env="DISPLAY=${DISPLAY}" \
	 --env="TERM=xterm-256color" \
	 -u $USER_ID:$GROUP_ID \
	 --privileged \
	 -v /sys/bus/pci/drivers:/sys/bus/pci/drivers \
	 -v /sys/kernel/mm/hugepages:/sys/kernel/mm/hugepages \
	 -v /sys/devices/system/node:/sys/devices/system/node \
	 -v /dev:/dev \
	 --net=host \
	 --workdir="$HOME" \
	 --name=$CONTAINER_NAME \
	 $REF_NAME
fi
