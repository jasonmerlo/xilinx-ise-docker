#!/bin/bash
IMAGE_NAME=$USER/ise_docker

sudo docker build \
     --build-arg USERNAME=$USER \
     --build-arg USERID=$(id -u) \
     --build-arg USERGROUP=$(id -g) \
     --network host \
     -t $IMAGE_NAME -f Dockerfile .
